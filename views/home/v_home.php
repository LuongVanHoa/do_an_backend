<div class="home-slides owl-carousel owl-theme">
    <div class="banner-item bg1">
        <div class="container">
            <div class="banner-item-content">
                <span class="sub-title">Super Offer</span>
                <h1>The Best Quality Organic Food</h1>
                <div class="price">
                    Price Only
                    <span>$95.00</span>
                </div>
                <a href="shop.php" class="default-btn"><span>Shop Now</span></a>
            </div>
        </div>
    </div>
    <div class="banner-item bg2">
        <div class="container">
            <div class="banner-item-content">
                <span class="sub-title">New Arrivals</span>
                <h1>Super Offer Pet Foods</h1>
                <div class="price">
                    Price Only
                    <span>$70.00</span>
                </div>
                <a href="shop.php" class="default-btn"><span>Shop Now</span></a>
            </div>
        </div>
    </div>
    <div class="banner-item bg3">
        <div class="container">
            <div class="banner-item-content">
                <span class="sub-title">Super Offer</span>
                <h1>Pet Toys Collection</h1>
                <div class="price">
                    Price Only
                    <span>$30.00</span>
                </div>
                <a href="shop.php" class="default-btn"><span>Shop Now</span></a>
            </div>
        </div>
    </div>
</div>


<div class="about-area pt-100 pb-75">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-3 col-md-12">
                <div class="about-image">
                    <img src="public/patoi/assets/img/about/about1.jpg" alt="about-image">
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="about-content">
                    <h2>About Patoi</h2>
                    <p>We offer quality products at low prices every day. We have been in this business for almost 20 years. We have been doing online shopping very confiden tly. Our customers like and trust us so much because of the quality of our site's products.</p>
                    <a href="shop.php" class="default-btn"><span>Shop Now</span></a>
                </div>
            </div>
            <div class="col-lg-3 col-md-12">
                <div class="about-image">
                    <img src="public/patoi/assets/img/about/about2.jpg" alt="about-image">
                </div>
            </div>
        </div>
    </div>
</div>


<div class="categories-area pb-75">
    <div class="container">
        <div class="section-title">
            <h2>Shop by Categories</h2>
        </div>
        <div class="categories-slides owl-carousel owl-theme">
            <div class="categories-box">
                <a href="shop.php" class="d-block">
                    <img src="public/patoi/assets/img/categories/categories1.jpg" alt="categories-image">
                    <h3>Dog food</h3>
                    <span>18 items</span>
                </a>
            </div>
            <div class="categories-box">
                <a href="shop.php" class="d-block">
                    <img src="public/patoi/assets/img/categories/categories2.jpg" alt="categories-image">
                    <h3>Cat food</h3>
                    <span>18 items</span>
                </a>
            </div>
            <div class="categories-box">
                <a href="shop.php" class="d-block">
                    <img src="public/patoi/assets/img/categories/categories3.jpg" alt="categories-image">
                    <h3>Pet accessories</h3>
                    <span>20 items</span>
                </a>
            </div>
            <div class="categories-box">
                <a href="shop.php" class="d-block">
                    <img src="public/patoi/assets/img/categories/categories4.jpg" alt="categories-image">
                    <h3>Dry food</h3>
                    <span>35 items</span>
                </a>
            </div>
            <div class="categories-box">
                <a href="shop.php" class="d-block">
                    <img src="public/patoi/assets/img/categories/categories5.jpg" alt="categories-image">
                    <h3>Pet toys</h3>
                    <span>12 items</span>
                </a>
            </div>
            <div class="categories-box">
                <a href="shop.php" class="d-block">
                    <img src="public/patoi/assets/img/categories/categories6.jpg" alt="categories-image">
                    <h3>Cat toys</h3>
                    <span>25 items</span>
                </a>
            </div>
        </div>
    </div>
</div>


<div class="products-area pb-75">
    <div class="container">
        <div class="section-title">
            <h2>New Arrivals</h2>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-3 col-md-12 col-sm-12">
                <div class="row">
                    <div class="col-lg-12 col-md-6 col-sm-6">
                        <div class="single-products-box">
                            <div class="image">
                                <a href="#" class="d-block">
                                    <img src="public/patoi/assets/img/products/products1.jpg" alt="products-image">
                                </a>
                                <ul class="products-button">
                                    <li><a href="cart.php"><i class='bx bx-cart-alt'></i></a></li>
                                    <li><a href="wishlist.php"><i class='bx bx-heart'></i></a></li>
                                    <li><a href="#" data-bs-toggle="modal" data-bs-target="#productsQuickView"><i class='bx bx-show'></i></a></li>
                                    <li><a href="products-details.php"><i class='bx bx-link-alt'></i></a></li>
                                </ul>
                            </div>
                            <div class="content">
                                <h3><a href="products-details.php">Pet brash</a></h3>
                                <div class="price">
                                    <span class="new-price">$35.00</span>
                                </div>
                                <div class="rating">
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bxs-star'></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-6 col-sm-6">
                        <div class="single-products-box">
                            <div class="image">
                                <a href="#" class="d-block">
                                    <img src="public/patoi/assets/img/products/products2.jpg" alt="products-image">
                                </a>
                                <span class="hot">Hot!</span>
                                <ul class="products-button">
                                    <li><a href="cart.php"><i class='bx bx-cart-alt'></i></a></li>
                                    <li><a href="wishlist.php"><i class='bx bx-heart'></i></a></li>
                                    <li><a href="#" data-bs-toggle="modal" data-bs-target="#productsQuickView"><i class='bx bx-show'></i></a></li>
                                    <li><a href="products-details.php"><i class='bx bx-link-alt'></i></a></li>
                                </ul>
                            </div>
                            <div class="content">
                                <h3><a href="products-details.php">Automatic dog blue leash</a></h3>
                                <div class="price">
                                    <span class="new-price">$75.00</span>
                                </div>
                                <div class="rating">
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bxs-star-half'></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12">
                <div class="offer-item">
                    <img src="public/patoi/assets/img/offer/offer14.png" alt="offer-image">
                    <h3><a href="products-details.php">Premium lamb rice</a></h3>
                    <span class="price">$240.00</span>
                    <div class="rating">
                        <i class='bx bxs-star'></i>
                        <i class='bx bxs-star'></i>
                        <i class='bx bxs-star'></i>
                        <i class='bx bxs-star'></i>
                        <i class='bx bxs-star'></i>
                    </div>
                    <p>Lorem ipsum dolor sit amet, consec tetur adipiscing elit, sed do eiusmod tempor.</p>
                    <h3>Place an order now</h3>
                    <span class="discount">Enjoy 30% OFF</span>
                    <div class="counter-class" data-date="2022-12-24 24:00:00">
                        <div><span class="counter-days"></span> Days</div>
                        <div><span class="counter-hours"></span> Hours</div>
                        <div><span class="counter-minutes"></span> Minutes</div>
                        <div><span class="counter-seconds"></span> Seconds</div>
                    </div>
                    <a href="shop.php" class="default-btn"><span>Shop Now</span></a>
                </div>
            </div>
            <div class="col-lg-3 col-md-12 col-sm-12">
                <div class="row">
                    <div class="col-lg-12 col-md-6 col-sm-6">
                        <div class="single-products-box">
                            <div class="image">
                                <a href="#" class="d-block">
                                    <img src="public/patoi/assets/img/products/products3.jpg" alt="products-image">
                                </a>
                                <ul class="products-button">
                                    <li><a href="cart.php"><i class='bx bx-cart-alt'></i></a></li>
                                    <li><a href="wishlist.php"><i class='bx bx-heart'></i></a></li>
                                    <li><a href="#" data-bs-toggle="modal" data-bs-target="#productsQuickView"><i class='bx bx-show'></i></a></li>
                                    <li><a href="products-details.php"><i class='bx bx-link-alt'></i></a></li>
                                </ul>
                            </div>
                            <div class="content">
                                <h3><a href="products-details.php">Cat toilet bowl</a></h3>
                                <div class="price">
                                    <span class="new-price">$49.00</span>
                                    <span class="old-price">$55.00</span>
                                </div>
                                <div class="rating">
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bx-star'></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-6 col-sm-6">
                        <div class="single-products-box">
                            <div class="image">
                                <a href="#" class="d-block">
                                    <img src="public/patoi/assets/img/products/products4.jpg" alt="products-image">
                                </a>
                                <span class="on-sale">On Sale!</span>
                                <ul class="products-button">
                                    <li><a href="cart.php"><i class='bx bx-cart-alt'></i></a></li>
                                    <li><a href="wishlist.php"><i class='bx bx-heart'></i></a></li>
                                    <li><a href="#" data-bs-toggle="modal" data-bs-target="#productsQuickView"><i class='bx bx-show'></i></a></li>
                                    <li><a href="products-details.php"><i class='bx bx-link-alt'></i></a></li>
                                </ul>
                            </div>
                            <div class="content">
                                <h3><a href="products-details.php">Bowl with rubber toy</a></h3>
                                <div class="price">
                                    <span class="new-price">$60.00</span>
                                </div>
                                <div class="rating">
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bxs-star'></i>
                                    <i class='bx bxs-star'></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="offer-area pb-75">
    <div class="container">
        <div class="single-offer-box">
            <a href="shop.php" class="d-block">
                <img src="public/patoi/assets/img/offer/offer8.jpg" alt="offer-image">
            </a>
        </div>
    </div>
</div>


<div class="products-area pb-75">
    <div class="container">
        <div class="section-title">
            <h2>Best Sellers</h2>
        </div>
        <div class="products-slides owl-carousel owl-theme">
            <div class="single-products-box">
                <div class="image">
                    <a href="#" class="d-block">
                        <img src="public/patoi/assets/img/products/products9.jpg" alt="products-image">
                    </a>
                    <ul class="products-button">
                        <li><a href="cart.php"><i class='bx bx-cart-alt'></i></a></li>
                        <li><a href="wishlist.php"><i class='bx bx-heart'></i></a></li>
                        <li><a href="#" data-bs-toggle="modal" data-bs-target="#productsQuickView"><i class='bx bx-show'></i></a></li>
                        <li><a href="products-details.php"><i class='bx bx-link-alt'></i></a></li>
                    </ul>
                    <div class="new">New!</div>
                </div>
                <div class="content">
                    <h3><a href="products-details.php">Pet chair</a></h3>
                    <div class="price">
                        <span class="new-price">$150.00</span>
                    </div>
                    <div class="rating">
                        <i class='bx bxs-star'></i>
                        <i class='bx bxs-star'></i>
                        <i class='bx bxs-star'></i>
                        <i class='bx bxs-star'></i>
                        <i class='bx bxs-star'></i>
                    </div>
                </div>
            </div>
            <div class="single-products-box">
                <div class="image">
                    <a href="#" class="d-block">
                        <img src="public/patoi/assets/img/products/products10.jpg" alt="products-image">
                    </a>
                    <ul class="products-button">
                        <li><a href="cart.php"><i class='bx bx-cart-alt'></i></a></li>
                        <li><a href="wishlist.php"><i class='bx bx-heart'></i></a></li>
                        <li><a href="#" data-bs-toggle="modal" data-bs-target="#productsQuickView"><i class='bx bx-show'></i></a></li>
                        <li><a href="products-details.php"><i class='bx bx-link-alt'></i></a></li>
                    </ul>
                </div>
                <div class="content">
                    <h3><a href="products-details.php">Pink ceramic cat bowl</a></h3>
                    <div class="price">
                        <span class="new-price">$39.00</span>
                    </div>
                    <div class="rating">
                        <i class='bx bxs-star'></i>
                        <i class='bx bxs-star'></i>
                        <i class='bx bxs-star'></i>
                        <i class='bx bxs-star'></i>
                        <i class='bx bxs-star'></i>
                    </div>
                </div>
            </div>
            <div class="single-products-box">
                <div class="image">
                    <a href="#" class="d-block">
                        <img src="public/patoi/assets/img/products/products11.jpg" alt="products-image">
                    </a>
                    <ul class="products-button">
                        <li><a href="cart.html"><i class='bx bx-cart-alt'></i></a></li>
                        <li><a href="wishlist.html"><i class='bx bx-heart'></i></a></li>
                        <li><a href="#" data-bs-toggle="modal" data-bs-target="#productsQuickView"><i class='bx bx-show'></i></a></li>
                        <li><a href="products-details.html"><i class='bx bx-link-alt'></i></a></li>
                    </ul>
                    <span class="sold">Sold!</span>
                </div>
                <div class="content">
                    <h3><a href="products-details.html">Red dog bed</a></h3>
                    <div class="price">
                        <span class="new-price">$125.00</span>
                        <span class="old-price">$145.00</span>
                    </div>
                    <div class="rating">
                        <i class='bx bxs-star'></i>
                        <i class='bx bxs-star'></i>
                        <i class='bx bxs-star'></i>
                        <i class='bx bxs-star-half'></i>
                        <i class='bx bx-star'></i>
                    </div>
                </div>
            </div>
            <div class="single-products-box">
                <div class="image">
                    <a href="#" class="d-block">
                        <img src="public/patoi/assets/img/products/products12.jpg" alt="products-image">
                    </a>
                    <ul class="products-button">
                        <li><a href="cart.html"><i class='bx bx-cart-alt'></i></a></li>
                        <li><a href="wishlist.html"><i class='bx bx-heart'></i></a></li>
                        <li><a href="#" data-bs-toggle="modal" data-bs-target="#productsQuickView"><i class='bx bx-show'></i></a></li>
                        <li><a href="products-details.html"><i class='bx bx-link-alt'></i></a></li>
                    </ul>
                </div>
                <div class="content">
                    <h3><a href="products-details.html">Pet carrier</a></h3>
                    <div class="price">
                        <span class="new-price">$39.00</span>
                    </div>
                    <div class="rating">
                        <i class='bx bxs-star'></i>
                        <i class='bx bxs-star'></i>
                        <i class='bx bxs-star'></i>
                        <i class='bx bxs-star'></i>
                        <i class='bx bxs-star-half'></i>
                    </div>
                </div>
            </div>
            <div class="single-products-box">
                <div class="image">
                    <a href="#" class="d-block">
                        <img src="public/patoi/assets/img/products/products1.jpg" alt="products-image">
                    </a>
                    <ul class="products-button">
                        <li><a href="cart.html"><i class='bx bx-cart-alt'></i></a></li>
                        <li><a href="wishlist.html"><i class='bx bx-heart'></i></a></li>
                        <li><a href="#" data-bs-toggle="modal" data-bs-target="#productsQuickView"><i class='bx bx-show'></i></a></li>
                        <li><a href="products-details.html"><i class='bx bx-link-alt'></i></a></li>
                    </ul>
                </div>
                <div class="content">
                    <h3><a href="products-details.html">Pet brash</a></h3>
                    <div class="price">
                        <span class="new-price">$35.00</span>
                    </div>
                    <div class="rating">
                        <i class='bx bxs-star'></i>
                        <i class='bx bxs-star'></i>
                        <i class='bx bxs-star'></i>
                        <i class='bx bxs-star'></i>
                        <i class='bx bxs-star'></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="facility-area pb-75">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                <div class="facility-box">
                    <img src="public/patoi/assets/img/icon/icon1.png" alt="icon">
                    <h3>Best collection</h3>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                <div class="facility-box bg-fed3d1">
                    <img src="public/patoi/assets/img/icon/icon2.png" alt="icon">
                    <h3>Fast delivery</h3>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                <div class="facility-box bg-a9d4d9">
                    <img src="public/patoi/assets/img/icon/icon3.png" alt="icon">
                    <h3>24/7 customer support</h3>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-6">
                <div class="facility-box bg-fef2d1">
                    <img src="public/patoi/assets/img/icon/icon4.png" alt="icon">
                    <h3>Secured payment</h3>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="products-area pb-75">
    <div class="container">
        <div class="section-title">
            <h2>Customer Favorites</h2>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="products-ads-box">
                    <a href="shop-grid.html"><img src="public/patoi/assets/img/products/products-ads2.jpg" alt="products-ads"></a>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="single-products-box">
                    <div class="image">
                        <a href="#" class="d-block">
                            <img src="public/patoi/assets/img/products/products16.jpg" alt="products-image">
                        </a>
                        <span class="hot">Hot!</span>
                        <ul class="products-button">
                            <li><a href="cart.html"><i class='bx bx-cart-alt'></i></a></li>
                            <li><a href="wishlist.html"><i class='bx bx-heart'></i></a></li>
                            <li><a href="#" data-bs-toggle="modal" data-bs-target="#productsQuickView"><i class='bx bx-show'></i></a></li>
                            <li><a href="products-details.html"><i class='bx bx-link-alt'></i></a></li>
                        </ul>
                    </div>
                    <div class="content">
                        <h3><a href="products-details.html">Traveling petcarrier</a></h3>
                        <div class="price">
                            <span class="new-price">$125.00</span>
                        </div>
                        <div class="rating">
                            <i class='bx bxs-star'></i>
                            <i class='bx bxs-star'></i>
                            <i class='bx bxs-star'></i>
                            <i class='bx bxs-star'></i>
                            <i class='bx bxs-star-half'></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="single-products-box">
                    <div class="image">
                        <a href="#" class="d-block">
                            <img src="public/patoi/assets/img/products/products6.jpg" alt="products-image">
                        </a>
                        <ul class="products-button">
                            <li><a href="cart.html"><i class='bx bx-cart-alt'></i></a></li>
                            <li><a href="wishlist.html"><i class='bx bx-heart'></i></a></li>
                            <li><a href="#" data-bs-toggle="modal" data-bs-target="#productsQuickView"><i class='bx bx-show'></i></a></li>
                            <li><a href="products-details.html"><i class='bx bx-link-alt'></i></a></li>
                        </ul>
                    </div>
                    <div class="content">
                        <h3><a href="products-details.html">Toy gnawing</a></h3>
                        <div class="price">
                            <span class="new-price">$15.00</span>
                            <span class="old-price">$20.00</span>
                        </div>
                        <div class="rating">
                            <i class='bx bxs-star'></i>
                            <i class='bx bxs-star'></i>
                            <i class='bx bxs-star'></i>
                            <i class='bx bxs-star'></i>
                            <i class='bx bx-star'></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="single-products-box">
                    <div class="image">
                        <a href="#" class="d-block">
                            <img src="public/patoi/assets/img/products/products17.jpg" alt="products-image">
                        </a>
                        <span class="on-sale">On Sale!</span>
                        <ul class="products-button">
                            <li><a href="cart.html"><i class='bx bx-cart-alt'></i></a></li>
                            <li><a href="wishlist.html"><i class='bx bx-heart'></i></a></li>
                            <li><a href="#" data-bs-toggle="modal" data-bs-target="#productsQuickView"><i class='bx bx-show'></i></a></li>
                            <li><a href="products-details.html"><i class='bx bx-link-alt'></i></a></li>
                        </ul>
                    </div>
                    <div class="content">
                        <h3><a href="products-details.html">Chihuahua dogs bed</a></h3>
                        <div class="price">
                            <span class="new-price">$155.00</span>
                        </div>
                        <div class="rating">
                            <i class='bx bxs-star'></i>
                            <i class='bx bxs-star'></i>
                            <i class='bx bxs-star'></i>
                            <i class='bx bxs-star'></i>
                            <i class='bx bxs-star'></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="feedback-area pb-100">
    <div class="container">
        <div class="section-title">
            <h2>What Our Client Says</h2>
        </div>
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-12">
                <div class="feedback-slides owl-carousel owl-theme">
                    <div class="single-feedback-box">
                        <p>“Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Nulla porttitor accumsan tincidunt. Vivamus magna justo, lacinia eget consectetur sed convallis at tellus.”</p>
                        <div class="client-info">
                            <img src="public/patoi/assets/img/user/user1.jpg" alt="user">
                            <h3>Kelly J. Brown</h3>
                            <span>Manager</span>
                        </div>
                    </div>
                    <div class="single-feedback-box">
                        <p>“Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Nulla porttitor accumsan tincidunt. Vivamus magna justo, lacinia eget consectetur sed convallis at tellus.”</p>
                        <div class="client-info">
                            <img src="public/patoi/assets/img/user/user2.jpg" alt="user">
                            <h3>Sarah Taylor</h3>
                            <span>Developer</span>
                        </div>
                    </div>
                    <div class="single-feedback-box">
                        <p>“Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Vivamus suscipit tortor eget felis porttitor volutpat. Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Nulla porttitor accumsan tincidunt. Vivamus magna justo, lacinia eget consectetur sed convallis at tellus.”</p>
                        <div class="client-info">
                            <img src="public/patoi/assets/img/user/user3.jpg" alt="user">
                            <h3>James Andy</h3>
                            <span>Designer</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 xol-md-12">
                <div class="video-box">
                    <img src="public/patoi/assets/img/video.jpg" alt="video-image">
                    <a href="https://www.youtube.com/watch?v=0O2aH4XLbto" class="popup-video"><i class='bx bx-play'></i></a>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="offer-area pb-75">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-6">
                <div class="single-offer-box">
                    <a href="shop-grid.html" class="d-block">
                        <img src="public/patoi/assets/img/offer/offer12.jpg" alt="offer-image">
                    </a>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="single-offer-box">
                    <a href="shop-grid.html" class="d-block">
                        <img src="public/patoi/assets/img/offer/offer13.jpg" alt="offer-image">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="partners-area">
    <div class="container">
        <div class="section-title">
            <h2>Our Partners</h2>
        </div>
        <div class="partners-inner">
            <div class="row justify-content-center align-items-center">
                <div class="col-lg-2 col-md-3 col-sm-4 col-4">
                    <div class="single-partners-box">
                        <a href="shop-grid.html" class="d-block">
                            <img src="public/patoi/assets/img/brands/brands1.png" alt="brands">
                        </a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4 col-4">
                    <div class="single-partners-box">
                        <a href="shop-grid.html" class="d-block">
                            <img src="public/patoi/assets/img/brands/brands2.png" alt="brands">
                        </a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4 col-4">
                    <div class="single-partners-box">
                        <a href="shop-grid.html" class="d-block">
                            <img src="public/patoi/assets/img/brands/brands3.png" alt="brands">
                        </a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4 col-4">
                    <div class="single-partners-box">
                        <a href="shop-grid.html" class="d-block">
                            <img src="public/patoi/assets/img/brands/brands4.png" alt="brands">
                        </a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4 col-4">
                    <div class="single-partners-box">
                        <a href="shop-grid.html" class="d-block">
                            <img src="public/patoi/assets/img/brands/brands5.png" alt="brands">
                        </a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4 col-4">
                    <div class="single-partners-box">
                        <a href="shop-grid.html" class="d-block">
                            <img src="public/patoi/assets/img/brands/brands6.png" alt="brands">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="blog-area pt-100 pb-75">
    <div class="container">
        <div class="section-title">
            <h2>Latest Blog</h2>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-4 col-md-6">
                <div class="single-blog-post">
                    <div class="image">
                        <a href="blog-details.html" class="d-block">
                            <img src="public/patoi/assets/img/blog/blog1.jpg" alt="blog-image">
                        </a>
                    </div>
                    <div class="content">
<span class="date">
<span>10 Aug</span> 2021
</span>
                        <a href="blog-grid.html" class="category">Training</a>
                        <h3><a href="blog-details.html">Properly a pet training guide</a></h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-blog-post">
                    <div class="image">
                        <a href="blog-details.html" class="d-block">
                            <img src="public/patoi/assets/img/blog/blog2.jpg" alt="blog-image">
                        </a>
                    </div>
                    <div class="content">
<span class="date">
<span>11 Aug</span> 2021
</span>
                        <a href="blog-grid.html" class="category">Behaviour</a>
                        <h3><a href="blog-details.html">The exact rules of how to travel with pets</a></h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single-blog-post">
                    <div class="image">
                        <a href="blog-details.html" class="d-block">
                            <img src="public/patoi/assets/img/blog/blog3.jpg" alt="blog-image">
                        </a>
                    </div>
                    <div class="content">
<span class="date">
<span>12 Aug</span> 2021
</span>
                        <a href="blog-grid.html" class="category">Solutions</a>
                        <h3><a href="blog-details.html">Creating proper guidelines for pet food </a></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>