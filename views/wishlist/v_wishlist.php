<div class="page-title-area">
    <div class="container">
        <div class="page-title-content">
            <h1>Wishlist</h1>
            <ul>
                <li><a href="home.php">Home</a></li>
                <li>Wishlist</li>
            </ul>
        </div>
    </div>
</div>


<div class="wishlist-area ptb-100">
    <div class="container">
        <form>
            <div class="wishlist-table table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">Product</th>
                        <th scope="col">Name</th>
                        <th scope="col">Unit Price</th>
                        <th scope="col">Stock Status</th>
                        <th scope="col">Shop Now</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <a href="products_details.php">
                                <img src="public/patoi/assets/img/products/products4.jpg" alt="item">
                            </a>
                        </td>
                        <td><a href="products-details.php">Pet chair</a></td>
                        <td class="product-price">$150.00</td>
                        <td class="product-stock-status">
                            <span class="in-stock"><i class='bx bx-check-circle'></i> In Stock</span>
                        </td>
                        <td>
                            <a href="cart.php" class="default-btn"><span>Shop Now</span></a>
                        </td>
                        <td><a href="wishlist.php" class="remove"><i class='bx bx-trash-alt'></i></a></td>
                    </tr>
                    <tr>
                        <td>
                            <a href="products-details.html">
                                <img src="assets/img/products/products5.jpg" alt="item">
                            </a>
                        </td>
                        <td><a href="products-details.html">Pink ceramic cat bowl</a></td>
                        <td class="product-price">$199.00</td>
                        <td class="product-stock-status">
                            <span class="in-stock"><i class='bx bx-check-circle'></i> In Stock</span>
                        </td>
                        <td>
                            <a href="cart.html" class="default-btn"><span>Shop Now</span></a>
                        </td>
                        <td><a href="wishlist.html" class="remove"><i class='bx bx-trash-alt'></i></a></td>
                    </tr>
                    <tr>
                        <td>
                            <a href="products-details.html">
                                <img src="assets/img/products/products6.jpg" alt="item">
                            </a>
                        </td>
                        <td><a href="products-details.html">Pet carrier</a></td>
                        <td class="product-price">$233.99</td>
                        <td class="product-stock-status">
                            <span class="in-stock"><i class='bx bx-check-circle'></i> In Stock</span>
                        </td>
                        <td>
                            <a href="cart.html" class="default-btn"><span>Shop Now</span></a>
                        </td>
                        <td><a href="wishlist.html" class="remove"><i class='bx bx-trash-alt'></i></a></td>
                    </tr>
                    <tr>
                        <td>
                            <a href="products-details.html">
                                <img src="assets/img/products/products7.jpg" alt="item">
                            </a>
                        </td>
                        <td><a href="products-details.html">Red dog bed</a></td>
                        <td class="product-price">$223.00</td>
                        <td class="product-stock-status">
                            <span class="out-stock"><i class='bx bx-x'></i> Out of Stock</span>
                        </td>
                        <td>
                            <a href="cart.html" class="default-btn"><span>Shop Now</span></a>
                        </td>
                        <td><a href="wishlist.html" class="remove"><i class='bx bx-trash-alt'></i></a></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </form>
    </div>
</div>
