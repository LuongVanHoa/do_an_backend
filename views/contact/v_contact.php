<div class="page-title-area">
    <div class="container">
        <div class="page-title-content">
            <h1>Contact Us</h1>
            <ul>
                <li><a href="home.php">Home</a></li>
                <li>Contact Us</li>
            </ul>
        </div>
    </div>
</div>


<div class="contact-area pt-100">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <div class="contact-form">
                    <h3>Get In Touch</h3>
                    <form id="contactForm">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="form-group mb-3">
                                    <label>Your Name</label>
                                    <input type="text" name="name" class="form-control" id="name" required data-error="Please enter your name">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="form-group mb-3">
                                    <label>Email Address</label>
                                    <input type="email" name="email" class="form-control" id="email" required data-error="Please enter your email">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="form-group mb-3">
                                    <label>Phone Number</label>
                                    <input type="text" name="phone_number" class="form-control" id="phone_number" required data-error="Please enter your phone number">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="form-group mb-3">
                                    <label>Subject</label>
                                    <input type="text" name="msg_subject" class="form-control" id="msg_subject" required data-error="Please enter your subject">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group mb-3">
                                    <label>Message...</label>
                                    <textarea name="message" id="message" class="form-control" cols="30" rows="4" required data-error="Please enter your message"></textarea>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-check mb-3">
                                    <input type="checkbox" class="form-check-input" id="checkme">
                                    <label class="form-check-label" for="checkme">
                                        Accept <a href="terms-conditions.php">Terms of Services</a> and <a href="privacy-policy.php">Privacy Policy</a>
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <button type="submit" class="default-btn"><span>Send Message</span></button>
                                <div id="msgSubmit" class="h3 text-center hidden"></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="contact-info">
                    <h3>Contact Information</h3>
                    <ul>
                        <li><span>Hotline:</span> <a href="tel:12855">0357143496</a></li>
                        <li><span>Tech support:</span> <a href="tel:+1514312-5678">0357143496</a></li>
                        <li><span>Email:</span> toiroiluomoi123@gmail.com</a></li>
                        <li><span>Address:</span> Tân Thành, Kiên Thành, Lục Ngạn, Bắc Giang</li>
                        <li><span>Available:</span> Monday - Friday 8:00am - 16:00pm</li>
                    </ul>
                </div>
            </div>
        </div>
        <div id="maps">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d9247.700554633266!2d105.71783169466616!3d21.05838347557668!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3134544e8224c0e7%3A0x722979785b893bf4!2zODUgxJDGsOG7nW5nIExhaSBYw6EsIMSQ4bupYyBHaWFuZywgSG_DoGkgxJDhu6ljLCBIw6AgTuG7mWksIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1647518858744!5m2!1svi!2s" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        </div>
    </div>
</div>
